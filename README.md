# Starter app for JavaScript development

## Co umíme
- dev server na HTTPS
- dev balík
- produkční balík
- kontrola JS
- kontrola CSS
- aliasy složek

## Instalace

 ```shell
 git clone git@gitlab.com:kurzy/js-dev-starter.git
 cd js-dev-starter
 npm install
 ```

## Důležité příkazy
- `npm start` - nastarování vývojového serveru (vč. watch) s podporou https (viz [cert readme](./cert/readme.md)), kontrola js a css souborů
- `npm run watch` - build vývojových souborů do složky `./dist` po každém uložení, kontrola js a css souborů
- `npm run dev` - build vývojových souborů do složky `./dist`, kontrola js a css souborů
- `npm run build` - build produkčních souborů do složky `./dist`, minimalizace, kontrola js a css souborů
- `npm run eslint` - kontrola js souborů podle produkční konfigurace
- `npm run eslintDev` - kontrola js souborů podle vývojové konfigurace
- `npm run stylelint` - kontrola css souborů
