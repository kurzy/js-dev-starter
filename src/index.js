import './css/index.css';
import { random, sum } from 'Utils/math';

const component = () => {
  const fragment = document.createDocumentFragment();
  const header = document.createElement('h1');
  header.textContent = 'Hello world! I\'m first prereact app.';

  console.log(header.textContent);
  console.log(random(2, 5));
  console.log(sum(1, 3, 5, 7));

  const infoElement = document.createElement('div');
  infoElement.textContent = `${PRODUCTION ? 'Jde ' : 'Nejde '} o produkční verzi`;

  fragment.appendChild(header);
  fragment.appendChild(infoElement);

  return fragment;
};

document.querySelector('#app').appendChild(component());
