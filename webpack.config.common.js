const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  entry: {
    index: './src/index.js',
  },
  target: 'web',
  resolve: {
    extensions: ['.js'],
    alias: {
      Utils: path.resolve(__dirname, 'src/utils/'),
    },
  },
  module: {
    rules: [],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'První React aplikace',
      template: './template/index.html',
    }),
    new CopyPlugin({
      patterns: [
        { from: './static', to: './' },
      ],
    }),
    new StylelintPlugin({
      files: '**/*.css',
    }),
  ],
};
